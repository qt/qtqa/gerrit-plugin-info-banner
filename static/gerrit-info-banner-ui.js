//
// Copyright (C) 2022 The Qt Company
//

'use strict';


Gerrit.install(plugin => {

    var bannerInfoElement = null;
    var bannerSettingElement = null;
    var bannerInfoMessage = "";
    var bannerInfoEditable = false;

    plugin.restApi().get('/config/server/gerrit-plugin-info-banner~message')
        .then((ok_resp) => {
            bannerInfoMessage = ok_resp.message;
            bannerInfoEditable = ok_resp.editable;
            if (bannerInfoElement) {
                  bannerInfoElement.textContent = bannerInfoMessage;
                  bannerInfoElement.hidden = (bannerInfoMessage != "" ? false : true);
            }
            if (bannerSettingElement && bannerInfoEditable) {
                bannerSettingElement.hidden = false;
            }
        }).catch((failed_resp) => {
            // use defaults
        });

    plugin.hook('banner').onAttached(element => {
        const el = document.createElement('div');
        el.textContent = bannerInfoMessage;
        el.style = `color:var(--warning-foreground);
                    background: var(--warning-background);
                    padding: var(--spacing-s) var(--spacing-l);
                    font-size: large;
                    text-align: center;`;
        el.hidden = (bannerInfoMessage != "" ? false : true);
        element.appendChild(el);
        bannerInfoElement = el;
    });

    plugin.hook('settings-screen').onAttached(element => {
        const html = `
          <h2>Banner Info Message</h2>
          <form is="iron-form">
            <textarea id="bannerMessage" placeholder="Empty" rows = "2"
                 style="width: 100%;
                 border: 1px solid var(--border-color);
                 border-radius: var(--border-radius);
                 padding: var(--spacing-s);
                 background-color: var(--view-background-color);
                 color: var(--primary-text-color);"></textarea>
            <gr-button class="show" id="buttonUpdateBanner" role="button" tabindex="0" aria-disabled="false">Update</gr-button>
          </form>`;

        const el = document.createElement('div');
        el.innerHTML = html;
        el.hidden = (bannerInfoEditable ? false : true);
        element.appendChild(el);
        bannerSettingElement = el;

        const buttonUpdate = element.querySelector('#buttonUpdateBanner');
        buttonUpdate.addEventListener('click', function onOpen() {
            buttonUpdate.disabled = true;
            var bannerMessage = element.querySelector('#bannerMessage').value;
            if (!bannerMessage) bannerMessage = "";

            plugin.restApi().post("/config/server/gerrit-plugin-info-banner~message", {
                message: bannerMessage
                }).then(() => {
                        buttonUpdate.disabled = false;
                        window.location.reload(true);
                }).catch((failed_resp) => {
                    this.dispatchEvent(
                        new CustomEvent('show-alert', {
                            detail: {message: failed_resp},
                            composed: true,
                            bubbles: true,
                        })
                    );
                });
        });
    });
});
