// Copyright (C) 2022-24 The Qt Company

package org.qtproject.codereview.gerritinfobanner;

import static com.google.common.truth.Truth.assertThat;
import static com.google.gerrit.acceptance.testsuite.project.TestProjectUpdate.allowCapability;
import static com.google.gerrit.server.group.SystemGroupBackend.REGISTERED_USERS;
import static com.google.gerrit.server.permissions.DefaultPermissionMappings.pluginCapabilityName;

import com.google.gerrit.acceptance.testsuite.project.ProjectOperations;
import com.google.gerrit.acceptance.LightweightPluginDaemonTest;
import com.google.gerrit.acceptance.RestResponse;
import com.google.gerrit.acceptance.TestPlugin;
import com.google.gerrit.extensions.api.access.PluginPermission;
import com.google.inject.Inject;
import org.junit.Test;

@TestPlugin(
    name = "gerrit-plugin-info-banner",
    sysModule = "org.qtproject.codereview.gerritinfobanner.PluginModule")
public class BannerMessageIT extends LightweightPluginDaemonTest {

  final String url = "/config/server/gerrit-plugin-info-banner~message";

  @Inject private ProjectOperations projectOperations;

  @Test
  public void get_Banner_Message_Empty() throws Exception {
    RestResponse response = userRestSession.get(url);
    response.assertOK();
    assertThat(response.getEntityContent()).contains("{\"message\":\"\",\"editable\":false}");

    response = adminRestSession.get(url);
    response.assertOK();
    assertThat(response.getEntityContent()).contains("{\"message\":\"\",\"editable\":true}");
  }

  @Test
  public void set_Banner_Message_By_Admin() throws Exception {
    BannerSettingInfo input = new BannerSettingInfo();
    input.message = "Maintenance break tomorrow";
    RestResponse response = adminRestSession.post(url, input);
    response.assertOK();

    response = userRestSession.get(url);
    response.assertOK();
    assertThat(response.getEntityContent())
        .contains("{\"message\":\"Maintenance break tomorrow\",\"editable\":false}");
  }

  @Test
  public void set_Banner_Message_By_Permission() throws Exception {
    projectOperations
        .allProjectsForUpdate()
        .add(
            allowCapability(
                    pluginCapabilityName(
                        new PluginPermission(
                            "gerrit-plugin-info-banner",
                            SetMessageCapability.SET_BANNER_MESSAGE)))
                .group(REGISTERED_USERS))
        .update();

    BannerSettingInfo input = new BannerSettingInfo();
    input.message = "Maintenance break tomorrow";
    RestResponse response = userRestSession.post(url, input);
    response.assertOK();

    response = userRestSession.get(url);
    response.assertOK();
    assertThat(response.getEntityContent())
        .contains("{\"message\":\"Maintenance break tomorrow\",\"editable\":false}");
  }

  @Test
  public void set_Banner_Message_No_Permission() throws Exception {
    BannerSettingInfo input = new BannerSettingInfo();
    input.message = "Maintenance break soon";
    RestResponse response = adminRestSession.post(url, input);
    response.assertOK();

    input.message = "Maintenance break over";
    response = userRestSession.post(url, input);
    assertThat(response.getEntityContent()).isEqualTo("not permitted");

    response = userRestSession.get(url);
    response.assertOK();
    assertThat(response.getEntityContent())
        .contains("{\"message\":\"Maintenance break soon\",\"editable\":false}");
  }
}
