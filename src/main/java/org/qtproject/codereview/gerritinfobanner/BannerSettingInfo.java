//
// Copyright (C) 2022 The Qt Company
//

package org.qtproject.codereview.gerritinfobanner;

public class BannerSettingInfo {
  String message;
  boolean editable;
}
