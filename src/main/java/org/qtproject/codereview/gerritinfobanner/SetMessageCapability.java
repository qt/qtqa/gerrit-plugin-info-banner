// Copyright (C) 2024 The Qt Company

package org.qtproject.codereview.gerritinfobanner;

import com.google.gerrit.extensions.config.CapabilityDefinition;

public class SetMessageCapability extends CapabilityDefinition {
  static final String SET_BANNER_MESSAGE = "setBannerMessage";

  @Override
  public String getDescription() {
    return "Update banner message";
  }
}
